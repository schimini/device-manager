# Device Manager

Device manager is a simple boilerplate rest api written in go. 
It features a CRUD controller for a generic device model, and a search endpoint. \
It uses mongoDB for storage and the docs for the API are available in the docs folder or via swagger ui through the `/swaggerui` endpoint.

# Installation / Usage

## Using docker

To build the docker image run `docker build --tag device-manager . `\
To run the application containerized run `docker-compose up --build ` or `docker compose up`

## Using Go Commands

`go run cmd/api/main.go` * \
`swag init -g pkg/api/api.go` (swagger docs generation) \
`air` (live reload) *


*requires MongoDB

# Test commands

`go test -coverprofile=coverage.out ./...`

`go tool cover -func=coverage.out`

# Requirements

- [Golang](https://go.dev/doc/install)

- [Mongodb](https://docs.mongodb.com/manual/administration/install-community/)

# Utilities

- postman collection

# Next Steps

- Add more tests