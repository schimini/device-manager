package main

import (
	"flag"
	"log"

	"github.com/joho/godotenv"
	"gitlab.com/schimini/device-manager/pkg/api"
	"gitlab.com/schimini/device-manager/utl/config"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println(".env file not loaded")
	}
	cfgPath := flag.String("p", "./cmd/api/conf.local.yaml", "Path to config file")
	flag.Parse()

	cfg, err := config.Load(*cfgPath)
	checkErr(err)

	checkErr(api.Start(cfg))
}

func checkErr(err error) {
	if err != nil {
		panic(err.Error())
	}
}
