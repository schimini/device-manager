package auth

import (
	"github.com/labstack/echo/v4"
)

// Middleware makes JWT implement the Middleware interface.
func Middleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			//token := c.Request().Header.Get("Authorization")
			/*if err != nil || !token.Valid {
				return c.NoContent(http.StatusUnauthorized)
			}

			claims := token.Claims.(jwt.MapClaims)

			id := int(claims["id"].(float64))
			companyID := int(claims["c"].(float64))
			locationID := int(claims["l"].(float64))
			username := claims["u"].(string)
			email := claims["e"].(string)
			//role := gorsk.AccessRole(claims["r"].(float64))

			c.Set("id", id)
			c.Set("company_id", companyID)
			c.Set("location_id", locationID)
			c.Set("username", username)
			c.Set("email", email)
			//c.Set("role", role)
			*/
			return next(c)
		}
	}
}
