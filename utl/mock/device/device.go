package mock

import (
	"fmt"

	"github.com/kamva/mgm/v3"
	"github.com/stretchr/testify/mock"
	"gitlab.com/schimini/device-manager/pkg/api/models"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MockDeviceModel struct {
	mock.Mock
}

func (dm MockDeviceModel) NewDevice(name string, brand string, t string) (*models.Device, error) {
	args := dm.Called(name, brand, t)
	return args.Get(0).(*models.Device), args.Error(1)
}

func (dm MockDeviceModel) Find(filter interface{}, opts ...*options.FindOptions) (*[]models.Device, error) {
	fmt.Println("egeg")
	args := dm.Called(filter, opts)
	return args.Get(0).(*[]models.Device), args.Error(1)
}

func (dm MockDeviceModel) FindByID(id interface{}, model mgm.Model) error {
	args := dm.Called(id, model)
	return args.Error(0)
}

func (dm MockDeviceModel) Update(model mgm.Model, opts ...*options.UpdateOptions) error {
	args := dm.Called(model, opts)
	return args.Error(0)
}

func (dm MockDeviceModel) Delete(model mgm.Model) error {
	args := dm.Called(model)
	return args.Error(0)
}
