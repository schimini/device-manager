package mongo

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func New(dbName string, mcs string, timeout int) error {
	var err error
	if timeout > 0 {
		err = mgm.SetDefaultConfig(&mgm.Config{CtxTimeout: time.Duration(timeout) * time.Second}, dbName, options.Client().ApplyURI(mcs))
	} else {
		err = mgm.SetDefaultConfig(&mgm.Config{CtxTimeout: 12 * time.Second}, dbName, options.Client().ApplyURI(mcs))
	}
	return err
}