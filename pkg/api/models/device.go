package models

import (
	"fmt"
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Device struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string    `json:"name" bson:"name"`
	Brand            string    `json:"brand" bson:"brand"`
	CreationTime     time.Time `json:"creation_time" bson:"creation_time"`
}
type GenericDeviceModel interface {
	NewDevice(string, string, string) (*Device, error)
	Find(interface{}, ...*options.FindOptions) (*[]Device, error)
	FindByID(interface{}, mgm.Model) error
	Update(mgm.Model, ...*options.UpdateOptions) error
	Delete(mgm.Model) error
}
type DeviceModel struct{}

func (dm DeviceModel) NewDevice(name string, brand string, t string) (*Device, error) {
	time, _ := time.Parse(time.RFC3339, t)
	d := &Device{
		Name:         name,
		Brand:        brand,
		CreationTime: time,
	}
	err := mgm.Coll(d).Create(d)
	return d, err
}

func (dm DeviceModel) Find(filter interface{}, opts ...*options.FindOptions) (*[]Device, error) {
	qresult := &[]Device{}
	err := mgm.Coll(&Device{}).SimpleFind(qresult, filter, opts...)
	return qresult, err
}

func (dm DeviceModel) FindByID(id interface{}, model mgm.Model) error {
	return mgm.Coll(&Device{}).FindByID(id, model)
}

func (dm DeviceModel) Update(model mgm.Model, opts ...*options.UpdateOptions) error {
	return mgm.Coll(&Device{}).Update(model, opts...)
}

func (dm DeviceModel) Delete(model mgm.Model) error {
	return mgm.Coll(&Device{}).Delete(model)
}

func InitDeviceIndex() {
	mod := mongo.IndexModel{Keys: bson.M{"brand": "text"}}
	ctx := mgm.Ctx()
	mgm.Coll(&Device{}).Indexes().DropAll(ctx)
	_, err := mgm.Coll(&Device{}).Indexes().CreateOne(ctx, mod)
	if err != nil {
		fmt.Println("Error creating index. Device search will not be available")
	}
}
