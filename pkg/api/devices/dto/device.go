package dto

import "time"

type CreateReq struct {
	Name         string `json:"name" validate:"required" example:"Example device"`
	Brand        string `json:"brand" validate:"required" example:"Example brand"`
	CreationTime string `json:"creation_time" validate:"datetime=2006-01-02T15:04:05Z07:00" example:"2020-12-01T16:20:00.000Z"`
}

type DeviceRes struct {
	Id           string    `json:"id" validate:"required"`
	Name         string    `json:"name" validate:"required" example:"Example device"`
	Brand        string    `json:"brand" validate:"required" example:"Example brand"`
	CreationTime time.Time `json:"creation_time" validate:"datetime=2006-01-02T15:04:05Z07:00" example:"2020-12-01T16:20:00.000Z"`
}

type UpdateReq struct {
	Name         string `json:"name,omitempty" example:"Example device"`
	Brand        string `json:"brand,omitempty" example:"Example brand"`
	CreationTime string `json:"creation_time,omitempty" validate:"omitempty,datetime=2006-01-02T15:04:05Z07:00" example:"2020-12-01T16:20:00.000Z"`
}

type DeviceListResponse struct {
	Devices []DeviceRes `json:"devices"`
	Page    int64       `json:"page" example:"0"`
}

type SearchReq struct {
	//Name         string `json:"name"  example:"Example"`
	Brand string `json:"brand"  example:"device"`
	//CreationTime string `json:"creation_time" validate:"datetime=2006-01-02T15:04:05Z07:00" example:"2020-12-01T16:20:00.000Z"`
}
