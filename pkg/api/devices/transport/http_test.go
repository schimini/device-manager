package transport_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/kamva/mgm/v3"
	"github.com/labstack/echo/v4/middleware"
	"github.com/stretchr/testify/assert"
	tmock "github.com/stretchr/testify/mock"
	device "gitlab.com/schimini/device-manager/pkg/api/devices"
	"gitlab.com/schimini/device-manager/pkg/api/devices/dto"
	dt "gitlab.com/schimini/device-manager/pkg/api/devices/transport"
	"gitlab.com/schimini/device-manager/pkg/api/models"
	mock "gitlab.com/schimini/device-manager/utl/mock/device"
	"gitlab.com/schimini/device-manager/utl/server"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func TestCreate(t *testing.T) {
	cases := []struct {
		name       string
		req        string
		wantStatus int
		wantResp   *dto.DeviceRes
	}{
		{
			name:       "Fail on validation (date)",
			req:        `{"name": "Test Device", "brand": "Test Brand", "creation_time": "22-04-23T18:25:43.511Z"}`,
			wantStatus: http.StatusBadRequest,
		},
		{
			name:       "Fail on validation",
			req:        `{"name": 2, "brand": "Test Brand", "creation_time": "22-04-23T18:25:43.511Z"}`,
			wantStatus: http.StatusBadRequest,
		},
		{
			name:       "Success",
			req:        `{"name": "Test Device", "brand": "Test Brand", "creation_time": "2021-04-20T18:25:43.511Z"}`,
			wantStatus: http.StatusOK,
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			// init
			r := server.New()
			rg := r.Group("api")

			// middleware
			r.Use(middleware.CORS())

			// mocks
			mockedModel := mock.MockDeviceModel{}
			time, _ := time.Parse(time.RFC3339, "2021-04-20T18:25:43.511Z")
			id, _ := primitive.ObjectIDFromHex("618bce250d77fe604c27ef1a")
			successReturn := &models.Device{Name: "Test Device", Brand: "Test Brand", CreationTime: time, DefaultModel: mgm.DefaultModel{
				IDField:    mgm.IDField{ID: id},
				DateFields: mgm.DateFields{},
			}}
			mockedModel.On("NewDevice", "Test Device", "Test Brand", "2021-04-20T18:25:43.511Z").Return(successReturn, nil)

			// routes
			dt.NewHTTP(device.New(mockedModel), rg)

			ts := httptest.NewServer(r)
			defer ts.Close()
			path := ts.URL + "/api/devices"
			fmt.Println(path)
			res, err := http.Post(path, "application/json", bytes.NewBufferString(tt.req))
			if err != nil {
				t.Fatal(err)
			}
			defer res.Body.Close()
			if tt.wantResp != nil {
				response := new(dto.DeviceRes)
				if err := json.NewDecoder(res.Body).Decode(response); err != nil {
					t.Fatal(err)
				}
				assert.Equal(t, tt.wantResp, response)
			}
			assert.Equal(t, tt.wantStatus, res.StatusCode)
		})
	}
}

func TestList(t *testing.T) {
	time, _ := time.Parse(time.RFC3339, "2021-04-20T18:25:43.511Z")
	cases := []struct {
		name       string
		req        string
		wantStatus int
		limit      int64
		offset     int64
		query      string
		wantResp   *dto.DeviceListResponse
	}{
		{
			name:       "Fail on validation (pagination)",
			query:      "?page=-1",
			wantStatus: http.StatusBadRequest,
		},
		{
			name:  "Success (pagination)",
			query: "?page=0&limit=2",
			limit: 2,
			wantResp: &dto.DeviceListResponse{Devices: []dto.DeviceRes{
				{Id: "aaaaaaaaaaaaaaaaaaaaaaaa", Name: "Test Device", Brand: "Test Brand", CreationTime: time},
				{Id: "aaaaaaaaaaaaaaaaaaaaaaab", Name: "Test Device 2", Brand: "Test Brand 2", CreationTime: time},
			}, Page: 0},
			wantStatus: http.StatusOK,
		},
		{
			name: "Success",
			wantResp: &dto.DeviceListResponse{Devices: []dto.DeviceRes{
				{Id: "aaaaaaaaaaaaaaaaaaaaaaaa", Name: "Test Device", Brand: "Test Brand", CreationTime: time},
				{Id: "aaaaaaaaaaaaaaaaaaaaaaab", Name: "Test Device 2", Brand: "Test Brand 2", CreationTime: time},
				{Id: "aaaaaaaaaaaaaaaaaaaaaaac", Name: "Test Device 3", Brand: "Test Brand 3", CreationTime: time},
			}, Page: 0},
			wantStatus: http.StatusOK,
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			// init
			r := server.New()
			rg := r.Group("api")

			// middleware
			r.Use(middleware.CORS())

			// mocks
			mockedModel := mock.MockDeviceModel{}
			id, _ := primitive.ObjectIDFromHex("aaaaaaaaaaaaaaaaaaaaaaaa")
			id2, _ := primitive.ObjectIDFromHex("aaaaaaaaaaaaaaaaaaaaaaab")
			id3, _ := primitive.ObjectIDFromHex("aaaaaaaaaaaaaaaaaaaaaaac")
			successReturn := []models.Device{
				{
					Name:         "Test Device",
					Brand:        "Test Brand",
					CreationTime: time,
					DefaultModel: mgm.DefaultModel{
						IDField:    mgm.IDField{ID: id},
						DateFields: mgm.DateFields{},
					},
				},
				{
					Name:         "Test Device 2",
					Brand:        "Test Brand 2",
					CreationTime: time,
					DefaultModel: mgm.DefaultModel{
						IDField:    mgm.IDField{ID: id2},
						DateFields: mgm.DateFields{},
					},
				},
				{
					Name:         "Test Device 3",
					Brand:        "Test Brand 3",
					CreationTime: time,
					DefaultModel: mgm.DefaultModel{
						IDField:    mgm.IDField{ID: id3},
						DateFields: mgm.DateFields{},
					},
				},
			}
			if tt.limit != 0 {
				successReturn = (successReturn)[0:tt.limit]
			}
			mockedModel.On("Find", tmock.Anything, tmock.Anything).Return(&successReturn, nil)

			// routes
			dt.NewHTTP(device.New(mockedModel), rg)

			ts := httptest.NewServer(r)
			defer ts.Close()
			path := ts.URL + "/api/devices" + tt.query
			fmt.Println(path)
			res, err := http.Get(path)
			if err != nil {
				t.Fatal(err)
			}
			defer res.Body.Close()
			if tt.wantResp != nil {
				response := new(dto.DeviceListResponse)
				if err := json.NewDecoder(res.Body).Decode(response); err != nil {
					t.Fatal(err)
				}
				assert.Equal(t, tt.wantResp, response)
			}
			assert.Equal(t, tt.wantStatus, res.StatusCode)
		})
	}
}
