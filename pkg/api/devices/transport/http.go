package transport

import (
	"net/http"

	"github.com/labstack/echo/v4"
	device "gitlab.com/schimini/device-manager/pkg/api/devices"
	"gitlab.com/schimini/device-manager/pkg/api/devices/dto"
	"gitlab.com/schimini/device-manager/utl/requests/pagination"
)

type HTTP struct {
	svc device.Service
}

func NewHTTP(svc device.Service, r *echo.Group) {
	h := HTTP{svc}
	dr := r.Group("/devices")
	dr.POST("", h.create)
	dr.GET("", h.list)
	dr.GET("/:id", h.view)
	dr.DELETE("/:id", h.delete)
	dr.PATCH("/:id", h.update)
	dr.POST("/search", h.search)
}

// CreateDevice godoc
// @Summary Create a device
// @Description creates a new device
// @Tags devices
// @Param device body dto.CreateReq true "Device info"
// @Success 200 {object} dto.DeviceRes
// @Router /devices [post]
func (h HTTP) create(c echo.Context) error {
	r := new(dto.CreateReq)

	if err := c.Bind(r); err != nil {

		return err
	}

	usr, err := h.svc.Create(c, *r)

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

// ListDevices godoc
// @Summary List devices
// @Description Lists devices
// @Tags devices
// @Param pagination query pagination.PaginationReq false "Pagination"
// @Success 200 {array} dto.DeviceRes
// @Router /devices [get]
func (h HTTP) list(c echo.Context) error {
	var req pagination.PaginationReq
	if err := c.Bind(&req); err != nil {
		return err
	}

	result, err := h.svc.List(c, req.Transform())

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, dto.DeviceListResponse{Devices: result, Page: req.Page})
}

// ViewDevice godoc
// @Summary Fetch a device
// @Description Fetches a device by id
// @Tags devices
// @Param id path string true "Device ID"
// @Success 200 {object} dto.DeviceRes
// @Router /devices/{id} [get]
func (h HTTP) view(c echo.Context) error {
	id := c.Param("id")

	result, err := h.svc.View(c, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, result)
}

// DeleteDevice godoc
// @Summary Delete a device
// @Description Deletes a device by id
// @Tags devices
// @Param id path string true "Device ID"
// @Success 200
// @Router /devices/{id} [delete]
func (h HTTP) delete(c echo.Context) error {
	id := c.Param("id")

	err := h.svc.Delete(c, id)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

// UpdateDevice godoc
// @Summary Update a device
// @Description Updates a device by id
// @Tags devices
// @Param id path string true "Device ID"
// @Param device body dto.UpdateReq true "Device partial/full info"
// @Success 200 {object} dto.DeviceRes
// @Router /devices/{id} [patch]
func (h HTTP) update(c echo.Context) error {
	r := new(dto.UpdateReq)
	if err := c.Bind(r); err != nil {
		return err
	}

	id := c.Param("id")
	result, err := h.svc.Update(c, id, *r)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, result)
}

// SearchDevice godoc
// @Summary Search devices
// @Description Searches a device by its properties
// @Tags devices
// @Param device body dto.SearchReq true "Device search info"
// @Success 200 {array} dto.DeviceRes
// @Router /devices/search [post]
func (h HTTP) search(c echo.Context) error {
	r := new(dto.SearchReq)
	if err := c.Bind(r); err != nil {
		return err
	}
	result, err := h.svc.Search(c, *r)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, result)
}
