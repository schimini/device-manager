package device

import (
	"time"

	"github.com/kamva/mgm/v3/operator"
	"github.com/labstack/echo/v4"
	"gitlab.com/schimini/device-manager/pkg/api/devices/dto"
	"gitlab.com/schimini/device-manager/pkg/api/models"
	"gitlab.com/schimini/device-manager/utl/requests/pagination"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type UserService struct {
	model models.GenericDeviceModel
}

func New(model models.GenericDeviceModel) UserService {
	return UserService{
		model,
	}
}

func (u UserService) Create(c echo.Context, d dto.CreateReq) (*dto.DeviceRes, error) {
	device, err := u.model.NewDevice(d.Name, d.Brand, d.CreationTime)
	if err != nil {
		return nil, err
	}
	return &dto.DeviceRes{Id: device.ID.Hex(), Name: device.Name, Brand: device.Brand, CreationTime: device.CreationTime}, err
}

func (u UserService) List(c echo.Context, p pagination.Pagination) ([]dto.DeviceRes, error) {
	result := []dto.DeviceRes{}
	qresult, err := u.model.Find(bson.M{}, &options.FindOptions{Limit: &p.Limit, Skip: &p.Offset})
	if err != nil {
		return result, err
	}
	for _, item := range *qresult {
		result = append(result, dto.DeviceRes{Id: item.ID.Hex(), Name: item.Name, Brand: item.Brand, CreationTime: item.CreationTime})
	}
	return result, err
}

func (u UserService) View(c echo.Context, id string) (*dto.DeviceRes, error) {
	device := &models.Device{}
	err := u.model.FindByID(id, device)
	if err != nil {
		return nil, err
	}
	return &dto.DeviceRes{Id: device.ID.Hex(), Name: device.Name, Brand: device.Brand, CreationTime: device.CreationTime}, nil
}

func (u UserService) Delete(c echo.Context, id string) error {
	device := &models.Device{}
	err := u.model.FindByID(id, device)
	if err != nil {
		return err
	}
	return u.model.Delete(device)
}

func (u UserService) Update(c echo.Context, id string, ur dto.UpdateReq) (*dto.DeviceRes, error) {
	device := &models.Device{}
	err := u.model.FindByID(id, device)
	if err != nil {
		return nil, err
	}
	device = merge(device, &ur)
	err = u.model.Update(device)
	if err != nil {
		return nil, err
	}
	return &dto.DeviceRes{Id: device.ID.Hex(), Name: device.Name, Brand: device.Brand, CreationTime: device.CreationTime}, nil
}

func (u UserService) Search(c echo.Context, sr dto.SearchReq) ([]dto.DeviceRes, error) {
	result := []dto.DeviceRes{}
	qresult, err := u.model.Find(bson.M{operator.Text: bson.M{"$search": sr.Brand}})
	if err != nil {
		return nil, err
	}
	for _, item := range *qresult {
		result = append(result, dto.DeviceRes{Id: item.ID.Hex(), Name: item.Name, Brand: item.Brand, CreationTime: item.CreationTime})
	}
	return result, nil
}

func merge(d *models.Device, ur *dto.UpdateReq) *models.Device {
	if ur.Name != "" {
		d.Name = ur.Name
	}
	if ur.Brand != "" {
		d.Brand = ur.Brand
	}
	if ur.CreationTime != "" {
		time, _ := time.Parse(time.RFC3339, ur.CreationTime)
		d.CreationTime = time
	}
	return d
}
