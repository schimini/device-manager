package device

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/schimini/device-manager/pkg/api/devices/dto"
	"gitlab.com/schimini/device-manager/utl/requests/pagination"
)

type Service interface {
	Create(echo.Context, dto.CreateReq) (*dto.DeviceRes, error)
	List(echo.Context, pagination.Pagination) ([]dto.DeviceRes, error)
	View(echo.Context, string) (*dto.DeviceRes, error)
	Delete(echo.Context, string) error
	Update(echo.Context, string, dto.UpdateReq) (*dto.DeviceRes, error)
	Search(echo.Context, dto.SearchReq) ([]dto.DeviceRes, error)
}
