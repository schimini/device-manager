// GORSK - Go(lang) restful starter kit
//
// API Docs for GORSK v1
//
// 	 Terms Of Service:  N/A
//     Schemes: http
//     Version: 2.0.0
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: Emir Ribic <ribice@gmail.com> https://ribice.ba
//     Host: localhost:8080
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - bearer: []
//
//     SecurityDefinitions:
//     bearer:
//          type: apiKey
//          name: Authorization
//          in: header
//
// swagger:meta
package api

import (
	"fmt"
	"os"

	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
	device "gitlab.com/schimini/device-manager/pkg/api/devices"
	dt "gitlab.com/schimini/device-manager/pkg/api/devices/transport"
	"gitlab.com/schimini/device-manager/pkg/api/models"
	"gitlab.com/schimini/device-manager/utl/config"
	authMw "gitlab.com/schimini/device-manager/utl/middleware/auth"
	"gitlab.com/schimini/device-manager/utl/mongo"
	"gitlab.com/schimini/device-manager/utl/server"

	_ "gitlab.com/schimini/device-manager/docs"
)

// @title Device Manager API
// @version 1.0
// @description This is a simple device manager server.

// @contact.name Pedro Cerejo
// @contact.url https://linkedin.com/in/pedrocerejo/
// @contact.email support@swagger.io

// @license.name MIT
// @license.url https://opensource.org/licenses/MIT

// @host localhost:8080
// @host localhost:80
// @BasePath /api/

// @accept json
// @produce json
// @schemes http
func Start(cfg *config.Configuration) error {
	// init
	err := mongo.New(
		os.Getenv("APP_DB_NAME"),
		fmt.Sprintf("mongodb://%s:%s@%s:%s/?authSource=admin",
			os.Getenv("MONGO_INITDB_ROOT_USERNAME"),
			os.Getenv("MONGO_INITDB_ROOT_PASSWORD"),
			os.Getenv("APP_DB_HOST"),
			os.Getenv("APP_DB_PORT")),
		cfg.DB.Timeout)
	if err != nil {
		return err
	}
	models.InitDeviceIndex()
	e := server.New()

	// swagger
	e.GET(cfg.App.SwaggerUIPath, echoSwagger.WrapHandler)

	// middlewares
	e.Use(middleware.CORS()) //TODO: Change cors to a less permissive setting or env based
	authMiddleware := authMw.Middleware()
	e.Use(authMiddleware)

	// routes
	ag := e.Group("api")
	dt.NewHTTP(device.New(models.DeviceModel{}), ag)

	server.Start(e, &server.Config{
		Port:                cfg.Server.Port,
		ReadTimeoutSeconds:  cfg.Server.ReadTimeout,
		WriteTimeoutSeconds: cfg.Server.WriteTimeout,
		Debug:               cfg.Server.Debug,
	})

	return nil
}
