# syntax=docker/dockerfile:1

FROM golang:1.16-alpine


WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY cmd ./cmd
COPY pkg ./pkg
COPY utl ./utl

RUN go get -u github.com/swaggo/swag/cmd/swag
RUN $GOPATH/bin/swag init -g pkg/api/api.go

RUN go build -o /device-manager cmd/api/main.go


CMD [ "/device-manager" ]